﻿function retrieveBasicSettings() {
    getBasicSettings(function(reply) {
        var courtName = reply.CourtName;
        $("#court-input").val(reply.CourtName);
        $("#club-input").val(reply.ClubName);
        $("#node-input").val(reply.NodeName);
        $("#node-identifier").val(reply.NodeIdentifier);
        $("#version-input").val(reply.Version);
        document.title = "CourtSide - " + reply.ClubName + ' - ' + courtName;
    });
}

function saveBasicSettings() {
    postBasicSettings($("#node-input").val(),
        $("#court-input").val(),
        $("#club-input").val(),
        function() {
            retrieveBasicSettings();
        });
}

function retrieveGameSettings() {
    getGameSettings(function (reply) {

        var selected = reply.MatchType;

        if (selected === "TableTennis") {
            selected = "Table tennis";
        }

        $("#gametype-button").html(selected);
    });
}

function saveGameSettings(gameType) {
    postGameSettings(gameType,
        function() {
            retrieveGameSettings();
        });
}

function retrieveLiveMode() {
    getLiveMode(function (reply) {        
        $("#livemode-button").html(reply.Mode);
    });
}

function saveLiveMode(liveMode) {
    postLiveMode(liveMode,
        function () {
            retrieveLiveMode();
        });
}

function retrieveRfidSettings() {
    getRfidSettings(function (reply) {
        $("#rfid-enabled").prop("checked", reply.IsEnabled);
        $("#rfid-address").val(reply.Address);
        $("#rfid-port").val(reply.Port);
    });
}

function retrieveChafonSettings() {
    getChafonSettings(function (reply) {

        var select1 = $("#rfid-usb1-select");
        fillSelect(select1, reply.AvailablePorts, reply.Devices[0]);

        var select2 = $("#rfid-usb2-select");
        fillSelect(select2, reply.AvailablePorts, reply.Devices[1]);
    });
}

function fillSelect(select, options, selected) {
    select.empty();

    select.append($("<option/>").text("disabled").attr("value", ""));

    for (var index in options) {
        var value = options[index];
        select.append($("<option/>").text(value).attr("value", value));
    }

    select[0].value = selected;
}

function saveRfidSettings() {
    postRfidSettings(
        $("#rfid-enabled").prop("checked"),
        $("#rfid-address").val(),
        $("#rfid-port").val(),
        function() {
            retrieveRfidSettings();
        });

    var devices = [$("#rfid-usb1-select").val(), $("#rfid-usb2-select").val()];
    
    postChafonSettings(devices,
        function() {
            retrieveChafonSettings();
        });
}

function retrieveSensorSettings() {
    getSensorSettings(function(reply) {
        var container = $("#reader-settings-container");

        var recording = false;
        container.empty();

        for (var index in reply.Sensors) {
            var mainDiv = $('<div class="form-group row"/>');
            var columnDiv = $('<div class="col-md-12"/>');
            var groupDiv = $('<div class="input-group"/>');
            mainDiv.append(columnDiv);
            columnDiv.append(groupDiv);

            var buttonDiv = $('<div class="col-xs-4"/>');
            var button = $('<div class="input-group-btn"> \
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">' +
                reply.Sensors[index].Purpose +
                '<span class="caret"></span> \
                                        </button> \
                                        <div class="dropdown-menu"> \
                                            <a class="dropdown-item" href="javascript: setPurpose(' +
                index +
                ', \'None\');">None</a> \
                                            <a class="dropdown-item" href="javascript: setPurpose(' +
                index +
                ', \'Win\');">Win</a> \
                                            <a class="dropdown-item" href="javascript: setPurpose(' +
                index +
                ', \'Correct\');">Correct</a> \
                                            <a class="dropdown-item" href="javascript: setPurpose(' +
                index +
                ', \'Lose\');">Lose</a> \
                                        </div> \
                                    </div>"');

            buttonDiv.append(button);

            var input =
                $('<input type="text" id="input2-group2" name="input2-group2" class="form-control" placeholder="Reader identifier" disabled="disabled">');

            var recordIndex = index;

            if (reply.Recording == index) {
                recordIndex = -1;
                recording = true;
            }

            var record = $('<button type="button" class="btn" alt="Record" onclick="javascript: recordReader(' +
                recordIndex +
                ');"><i class="fa fa-dot-circle-o"></i></button>');

            var clear =
                $('<button type="button" class="btn btn-warning" alt="Clear" onclick="javascript: clearReader(' +
                    recordIndex +
                    ');"><i class="fa fa-trash"></i></button>');

            input.val(reply.Sensors[index].Trigger);

            if (reply.Recording == index) {
                record.addClass("btn-success");
            } else {
                record.addClass("btn-danger");
            }

            if (reply.Sensors[index].Trigger == "") {
                clear.attr('disabled', 'disabled');
            }
            groupDiv.append(buttonDiv);
            groupDiv.append(input);
            groupDiv.append(record);
            groupDiv.append(clear);
            container.append(mainDiv);
        }

        if (recording) {
            setTimeout(retrieveSensorSettings, 1000);
        }
    });
}

function retrieveLastTags() {
    getLastTags(function(reply) {
        var container = $("#tag-table-body");

        container.empty();

        for (var i in reply) {
            var event = reply[i];

            var row = $("<tr/>");

            var timeCell = $("<td/>");
            var identifierCell = $("<td/>");
            var readerCell = $("<td/>");

            var date = new Date(event.Time);
            var time = date.toLocaleTimeString();

            timeCell.append(time);
            identifierCell.append(event.Tag);
            readerCell.append(event.Reader);

            row.append(timeCell);
            row.append(identifierCell);
            row.append(readerCell);

            container.append(row);
            if (i > 5) {
                break;
            }
        }

        setTimeout(retrieveLastTags, 1000);
    });
}

function fillPlayer(player, element) {
    if (player == null) {
        element.empty();
    } else {
        element.html(inSpan(player.Name));
    }
}

function recordReader(index) {
    postSensorRecord(index,
        function() {
            retrieveSensorSettings();
        });
}

function clearReader(index) {
    postClearSensor(index,
        function() {
            retrieveSensorSettings();
        });
}

function setPurpose(index, purpose) {
    postSensorPurpose(index,
        purpose,
        function() {
            retrieveSensorSettings();
        });
}

var lastClub = null;

function handleLinkClubs(clubs) {
    var select = $("#link-club-select");

    select.empty();


    if (clubs.length > 0) {
        select.append($("<option/>").text("Select a club...").attr("value", 0));

        for (var i in clubs) {
            var club = clubs[i];

            var clubElement = $("<option/>").text(club.Name).attr("value", club.Id);
            select.append(clubElement);
        }
        select.removeAttr("disabled");
    } else {
        select.attr("disabled", "disabled");
    }
}

function handleLinkLocations(club) {
    lastClub = club;

    var select = $("#link-location-select");

    select.empty();

    if (club && club.Locations.length > 0) {
        select.append($("<option/>").text("Select a location...").attr("value", 0));
        for (var i in club.Locations) {
            var location = club.Locations[i];

            var clubElement = $("<option/>").text(location.Identity.Name).attr("value", location.Identity.Id);
            select.append(clubElement);
        }
        select.removeAttr("disabled");
    } else {
        handleLinkCourts(null);
        select.attr("disabled", "disabled");
    }

}

function handleLinkCourts(location) {
    var select = $("#link-court-select");

    select.empty();

    if (location && location.Courts.length > 0) {
        select.append($("<option/>").text("Select a court...").attr("value", 0));
        for (var i in location.Courts) {
            var court = location.Courts[i];

            var element = $("<option/>").text(court.Name).attr("value", court.Id);
            select.append(element);
        }
        select.removeAttr("disabled");
    } else {
        handleCourtSelect(null);
        select.attr("disabled", "disabled");
    }
}

function handleCourtSelect(courtSelect) {
    var button = $("#save-link-button");

    if (courtSelect == null || courtSelect[0].value == "0") {
        button.attr("disabled", "disabled");
        return;
    }

    button.removeAttr("disabled");
}

function setupLinkSearch() {
    var inputBox = $("#link-club-name");
    inputBox.on("input",
        function() {
            if (inputBox[0].value.length < 3) {
                return;
            }

            getClubs(inputBox[0].value, handleLinkClubs);
        });

    var clubSelect = $("#link-club-select");
    var locationSelect = $("#link-location-select");
    var courtSelect = $("#link-court-select");

    inputBox.bind("keypress",
        function(e) {
            if (e.keyCode == 13) {
                clubSelect.focus();
                return false;
            }
            return true;
        });

    clubSelect.change(function() {
        var clubId = clubSelect[0].value;

        if (clubId == "0") {
            handleLinkLocations(null);
            return;
        }

        getClubDetails(clubId, handleLinkLocations);
    });

    locationSelect.change(
        function() {
            var locationId = locationSelect[0].value;

            if (locationId == "0" || lastClub == null) {
                handleLinkCourts(null);
                return;
            }

            for (var i in lastClub.Locations) {
                var location = lastClub.Locations[i];

                if (location.Identity.Id == locationId) {
                    handleLinkCourts(location);
                    break;
                }
            }
        });

    courtSelect.change(function() {
        handleCourtSelect(courtSelect);
    });
}

function saveLink() {
    var clubId = $("#link-club-select")[0].value;
    var locationId = $("#link-location-select")[0].value;
    var courtId = $("#link-court-select")[0].value;

    postSaveLink(clubId, locationId, courtId, retrieveBasicSettings);
}

retrieveBasicSettings();
retrieveGameSettings();
retrieveSensorSettings();
retrieveRfidSettings();
retrieveChafonSettings();
retrieveLastTags();
retrieveLiveMode();

setupLinkSearch();