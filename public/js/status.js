﻿
function inSpan(content) {
    var result = $("<span/>");
    result.append(content);
    return result;
}

function inRow(content) {
    var result = $("<div class='row'/>");
    result.append(content);
    return result;
}


function retrieveMatchStatus() {
    getMatchStatus(function(reply) {
        var mode = $("#match-status-mode");
        mode.empty();
        mode.append(inSpan(reply.Mode));

        var table = $("#match-status-table");

        if (reply.Mode != "Waiting") {

            if (reply.Match != null) {
                table.empty();

                var firstRow = $("<tr/>");
                var secondRow = $("<tr/>");
                for (var i in reply.Match.Sets) {
                    var set = reply.Match.Sets[i];

                    var cellA = $("<td/>");
                    var cellB = $("<td/>");

                    cellA.append(set.Score.A);
                    cellB.append(set.Score.B);

                    firstRow.append(cellA);
                    secondRow.append(cellB);
                }

                if (reply.Match.IsCompleted != true) {
                    var gameA = $("<td/>");
                    var gameB = $("<td/>");
                    var game = reply.Match.Score;

                    gameA.append(game.A);
                    gameB.append(game.B);

                    firstRow.append(gameA);
                    secondRow.append(gameB);
                }

                table.append(firstRow);
                table.append(secondRow);
            }
        } else {
            table.empty();
        }

        fillPlayer(reply.PlayerB, $("#match-status-player-b"));
        fillPlayer(reply.PlayerA, $("#match-status-player-a"));

        setTimeout(retrieveMatchStatus, 500);
    });
}

function retrieveUplinkStatus() {
    getUplinkStatus(function(reply) {
        var status = $("#uplink-status");
        var card = $("#uplink-status-card");
        status.empty();
        status.append(reply.Status);

        if (reply.Status == "Disconnected") {
            card.removeClass("card-primary");
            card.removeClass("card-warning");
            card.addClass("card-danger");
            card.removeClass("card-success");
        } else if (reply.Status == "Connected") {
            card.removeClass("card-primary");
            card.removeClass("card-warning");
            card.removeClass("card-danger");
            card.addClass("card-success");
        } else {
            card.removeClass("card-primary");
            card.addClass("card-warning");
            card.removeClass("card-danger");
            card.removeClass("card-success");
        }

        setTimeout(retrieveUplinkStatus, 5000);
    });
}

function retrieveDisplayStatus() {
    getDisplayStatus(function(reply) {
        var status = $("#display-status");
        var information = $("#display-status-information");
        var card = $("#display-status-card");
        storeStatus(reply, status, information, card);

        setTimeout(retrieveDisplayStatus, 5000);
    });
}

function retrieveRfidStatus() {
    getRfidStatus(function(reply) {
        var status = $("#rfid-status");
        var information = $("#rfid-status-information");
        var card = $("#rfid-status-card");
        storeStatus(reply, status, information, card);

        setTimeout(retrieveRfidStatus, 5000);
    });
}

function storeStatus(reply, status, information, card) {

    status.empty();
    status.append(reply.Status);
    information.empty();
    information.append(reply.Information);

    if (reply.Status == "Disconnected") {
        card.removeClass("card-primary");
        card.removeClass("card-warning");
        card.addClass("card-danger");
        card.removeClass("card-success");
    } else if (reply.Status == "Connected") {
        card.removeClass("card-primary");
        card.removeClass("card-warning");
        card.removeClass("card-danger");
        card.addClass("card-success");
    } else {
        card.removeClass("card-primary");
        card.addClass("card-warning");
        card.removeClass("card-danger");
        card.removeClass("card-success");
    }
}

function fillPlayer(player, element) {
    if (player == null) {
        element.empty();
    } else {
        element.html(inSpan(player.Name));
    }
}


function handleLinkLocations(club) {
    lastClub = club;

    var select = $("#link-location-select");

    select.empty();

    if (club && club.Locations.length > 0) {
        select.append($("<option/>").text("Select a location...").attr("value", 0));
        for (var i in club.Locations) {
            var location = club.Locations[i];

            var clubElement = $("<option/>").text(location.Identity.Name).attr("value", location.Identity.Id);
            select.append(clubElement);
        }
        select.removeAttr("disabled");
    } else {
        handleLinkCourts(null);
        select.attr("disabled", "disabled");
    }

}

function handleLinkCourts(location) {
    var select = $("#link-court-select");

    select.empty();

    if (location && location.Courts.length > 0) {
        select.append($("<option/>").text("Select a court...").attr("value", 0));
        for (var i in location.Courts) {
            var court = location.Courts[i];

            var element = $("<option/>").text(court.Name).attr("value", court.Id);
            select.append(element);
        }
        select.removeAttr("disabled");
    } else {
        handleCourtSelect(null);
        select.attr("disabled", "disabled");
    }
}


function setWirelessStatus(reply) {
    var checkBox = $("#wireless-status")[0];

    if (reply.Use == true) {
        checkBox.checked = true;
    } else {
        checkBox.checked = false;
    }
}

function retrieveWirelessSettings() {
    getWirelessStatus(function(reply) {
        setWirelessStatus(reply);
        var checkBox = $("#wireless-status");
        checkBox.on("change",
            function() {
                if (checkBox[0].checked) {
                    enableWireless(setWirelessStatus);
                } else {
                    disableWireless(setWirelessStatus);
                }
            });
    });
}

retrieveMatchStatus();
retrieveWirelessSettings();
retrieveUplinkStatus();
retrieveDisplayStatus();
retrieveRfidStatus();