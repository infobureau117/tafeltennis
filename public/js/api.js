﻿
function getBasicSettings(onBasicSettings) {
    $.get("api/settings/basic",
        onBasicSettings,
        "json");
};

function postBasicSettings(nodeName, courtName, clubName, action) {
    var settings = {
        NodeName: nodeName,
        CourtName: courtName,
        ClubName: clubName
    };

    JsonPost("/api/settings/basic", settings, action);
}

function getGameSettings(onBasicSettings) {
    $.get("api/settings/game",
        onBasicSettings,
        "json");
};

function postGameSettings(matchType, action) {
    var settings = { MatchType: matchType };

    JsonPost("api/settings/game", settings, action);
};

function getRfidSettings(onReply) {
    $.get("api/settings/rfid",
        onReply,
        "json");
};

function getChafonSettings(onReply) {
    $.get("api/settings/chafon",
        onReply,
        "json");
};

function getLiveMode(action) {
    $.get("api/settings/live",
        action,
        "json");
};

function postLiveMode(mode, action) {    
    $.post("api/settings/live/" + mode,
        action,
        "json");
};

function postRfidSettings(enabled, address, port, action) {
    var settings = {
        IsEnabled : enabled,
        Address: address,
        Port: port
    };

    JsonPost("api/settings/rfid", settings, action);
};

function postChafonSettings(devices, action) {
    var settings = {
        Devices: devices        
    };

    JsonPost("api/settings/chafon", settings, action);
};

function resetChafon(onComplete) {
    $.post("api/settings/chafon/reset",
        onComplete,
        "json");
};

function getLastTags(onLastTags) {
    $.get("api/tags/last10",
        onLastTags,
        "json");
};

function getMatchStatus(onMatchStatus) {
    $.get("api/match/status",
        onMatchStatus,
        "json");
};

function getUplinkStatus(onUplinkStatus) {
    $.get("api/network/uplink",
        onUplinkStatus,
        "json");
};

function getDisplayStatus(onDisplayStatus) {
    $.get("api/network/display",
        onDisplayStatus,
        "json");
};

function getRfidStatus(onRfidStatus) {
    $.get("api/network/rfid",
        onRfidStatus,
        "json");
};

function getLocalAddress(onReply) {
    $.get("api/network/localAddress",
        onReply,
        "json");
}

function JsonPost(url, value, action) {
    var jsonContent = JSON.stringify(value);

    $.ajax({
        type: "POST",
        url: url,
        data: jsonContent,
        dataType: "json",
        contentType: "application/json"
    }).done(action);
}

function getSensorSettings(onSensorSettings) {
    $.get("api/settings/sensor",
        onSensorSettings,
        "json");
};

function getClubs(text, onClubs) {
    $.get("api/court/clubs/" + text,
        onClubs,
        "json");
};

function getClubDetails(text, onClubDetails) {
    $.get("api/court/clubdetails/" + text,
        onClubDetails,
        "json");
};

function postSensorRecord(index, onComplete) {
    $.post("api/settings/sensor/record/" + index,
        onComplete,
        "json");
};

function postSensorPurpose(index, value, onComplete) {
    $.post("api/settings/sensor/purpose/" + index + "/" + value,
        onComplete,
        "json");
};

function postClearSensor(index, onComplete) {
    $.post("api/settings/sensor/clear/" + index,
        onComplete,
        "json");
}

function postSaveLink(club, location, court, onComplete) {

    var object = {
        Club: club,
        Location: location,
        Court: court
    }

    JsonPost("api/court/savelink", object, onComplete);
}

function resetMatch() {
    $.post("api/match/reset",
        null,
        "json");
}

function getWirelessStatus(onComplete) {
    $.get("api/network/wireless",
        onComplete,
        "json");
}

function enableWireless(onComplete) {
    $.post("api/network/wireless/enable",
        onComplete,
        "json");
}

function disableWireless(onComplete) {
    $.post("api/network/wireless/disable",
        onComplete,
        "json");
}