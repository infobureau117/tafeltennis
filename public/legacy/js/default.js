var RequestCourtInformation = function() {
    $.get("Court",
        function(reply) {
            var courtName = reply.CourtInformation.Court.Name;
            var clubName = reply.CourtInformation.Club.Name;
            $("#courtName").html(courtName);
            $("#linkHome").html(courtName);
            $("#clubName").html(clubName);

            $("#input-courtName").val(courtName);
            $("#input-clubName").val(clubName);

            $("#courtName").fadeIn();

            $("#clubName").fadeIn();

            document.title = clubName + ' - ' + courtName;
        },
        "json");
};

var HideCourtInformation = function(complete) {
    $("#courtName").fadeOut();
    $("#clubName").fadeOut(400, complete);
};

var createDiv = function() {
    return $('<div></div>');
};

var createDivWithContent = function(content) {
    return $('<div>' + content + '</div>');
};

var createColumn = function() {
    return $('<td></td>');
};

var createInput = function() {
    return $('<input></input>');
};

var createLabel = function(content) {
    return $('<label>' + content + '</label>');
};