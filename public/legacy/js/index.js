var RequestMatchStatus = function() {
    $.get("matchstatus",
            function(reply) {
                var mode = reply.MatchStatus.Mode;

                var statusString = getStringForMode(mode);
                if (statusString != null) {
                    $("#mainScoreContainer").fadeOut();
                    $('#content-waiting').html(statusString).fadeIn();
                } else {
                    $("#content-waiting").fadeOut();

                    var table = buildMatchScoreTable(GetMainMatchStatus(reply));

                    $("#mainScoreContainer").children().remove();
                    $("#mainScoreContainer").append(table);
                    $("#mainScoreContainer").fadeIn();
                }

                $('#scoreBox').removeClass('hidden').addClass('visible');
            },
            "json")
        .done(function() {
            setTimeout(RequestMatchStatus, 500);
        });
};

var getStringForMode = function(mode) {
    if (mode == "Waiting")
        return "Waiting for players.";

    if (mode == "RegisteringPlayers")
        return "Waiting for one more player.";

    if (mode == "PickServe")
        return "The players are deciding who should start serving.";

    return null;
};

var requestCourtsInformation = function() {
    $.get("courts",
            function(reply) {
                var courts = reply.Courts.Courts;

                $("#courtsOverviewList").children().remove();

                for (var i = 0; i < courts.length; i++) {
                    var court = courts[i];

                    var element = $('<li></li>');

                    var box = buildCourtsOverViewBox(court);

                    if (box)
                        box.appendTo(element);

                    element.appendTo('#courtsOverviewList');
                }
            },
            "json")
        .done(function() {
            setTimeout(requestCourtsInformation, 5000);
        });
};

var buildCourtsOverViewBox = function(court) {
    var courtName = court.Court.Name;

    if (!courtName)
        return null;
    var main = createDiv().addClass('courtOverViewGroup');

    var header = createDiv().addClass('courtOverViewHeader').html(courtName);
    var content = createDiv().addClass('courtOverViewContent');

    header.appendTo(main);

    var mode = court.Match.Mode;

    var statusString = getStringForMode(mode);

    if (statusString != null)
        content.html(statusString);
    else {
        var score = buildMatchScoreTable(court.Match);

        if (score)
            score.appendTo(content);
    }

    content.appendTo(main);

    return main;
};


RequestCourtInformation();
RequestMatchStatus();
requestCourtsInformation();
startTab(0);
startSlideShow();
checkLogIn();