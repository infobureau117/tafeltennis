
var requestPlayers = function() {
    $.get("players",
        function(reply) {
            var players = reply.Players;
            $("#playersList").children().remove();

            for (var i = 0; i < players.length; i++) {
                var player = players[i];

                var element = $('<li></li>');
                var content = buildPlayer(player);
                content.appendTo(element);

                element.appendTo('#playersList');
            }
        },
        "json");
};

var buildPlayer = function(player) {
    var main = createDiv();
    var tag = createDivWithContent(player.Tags[0].Identifier);
    var input = createInput();
    var identifier = 'input_' + player.Tags[0].Identifier;
    input.attr("id", identifier);
    input.attr("type", "text");

    var button = createDiv();
    button.addClass('input-login-show').addClass('saveButton');

    button.click(function() {
        savePlayer(player.Tags[0].Identifier, input.val());
    });

    input.val(player.Name);
    tag.appendTo(main);
    input.appendTo(main);
    button.appendTo(main);

    return main;
};

var savePlayer = function(tag, name) {
    var data = {
        Player: {
            Tag: tag,
            Name: name
        }
    };

    $.post("saveplayer",
        JSON.stringify(data));
};

RequestCourtInformation();
requestPlayers();
checkLogIn();