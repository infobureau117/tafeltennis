var currentlyRecording = 'None';

var saveCourtInformation = function() {
    HideCourtInformation();
    var courtName = $('#input-courtName').val();
    var clubName = $('#input-clubName').val();

    var data = { Court: { ClubName: clubName, Name: courtName } };

    $.post('Court',
        JSON.stringify(data),
        function() {
            HideCourtInformation(RequestCourtInformation);
        },
        "json");
};

var requestReadersInformation = function() {
    $.get("readers",
        function(reply) {
            $('#input-wonA').val(reply.Readers.SideAWon);
            $('#input-lostA').val(reply.Readers.SideALost);
            $('#input-correctA').val(reply.Readers.SideACorrect);

            $('#input-wonB').val(reply.Readers.SideBWon);
            $('#input-lostB').val(reply.Readers.SideBLost);
            $('#input-correctB').val(reply.Readers.SideBCorrect);
        },
        "json");

    $.get("recordreader",
        function(reply) {
            currentlyRecording = reply.RecordReader.Reader;

            if (currentlyRecording === 'None')
                hideRecording();
            else {
                $('#record-' + currentlyRecording).addClass('recording');

                setTimeout(requestReadersInformation, 1000);
            }
        },
        "json");
};

var toggleRecording = function(reader) {
    if (currentlyRecording === reader)
        reader = 'None';

    var data = { RecordReader: { Reader: reader } };

    hideRecording();

    $.post("recordreader",
        JSON.stringify(data),
        function() {
            requestReadersInformation();
        });
};

var hideRecording = function() {
    $('.recordButton').removeClass('recording');
};

var resetMatch = function() {
    $.post("resetmatch", '{}');
};

RequestCourtInformation();
requestReadersInformation();
checkLogIn();