var logIn = function() {
    $('#login-dialog').dialog('close');
    var loginName = $('#loginName').val();
    var loginPassword = $('#loginPassword').val();

    var login = { User: loginName, Password: loginPassword };
    var dataObject = { LogIn: login };

    $.post('LogIn',
        JSON.stringify(dataObject),
        function(response) {
            var sessionIdentifier = response.LogIn.SessionIdentifier;

            if (sessionIdentifier != null)
                $.cookie('CourtSideLogIn', loginName + '.' + sessionIdentifier);

            $('#loginPassword').val('');
            checkLogIn();
        },
        "json");
};

var logOut = function() {
    $.cookie('CourtSideLogIn', '');

    checkLogIn();
};

var showLogInDialog = function() {
    $('#login-dialog')
        .dialog({
            modal: true
        });
};

var checkLogIn = function() {
    if (isLoggedIn()) {
        $('.login-show').show();
        $('.login-hide').hide();
        $('.input-login-show').prop('readonly', false);

        $('#login-link').hide();
    } else {
        $('.login-show').hide();
        $('.login-hide').show();
        $('.input-login-show').prop('readonly', true);

        $('#login-link').show();
    }
};

var isLoggedIn = function() {
    return getLogInName() != null;
};


var getLogInName = function() {
    var value = $.cookie('CourtSideLogIn');

    var splits = value.split('.', 3);

    if (splits.length == 2)
        return splits[0];

    return null;
};