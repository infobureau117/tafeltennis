﻿var buildMatchScoreTable = function(match) {
    var div = createDiv().addClass('scoreContainer');
    var table = $('<table></table>');
    var rowA = $('<tr></tr>').appendTo(table);
    var rowB = $('<tr></tr>').appendTo(table);

    var serving = match.Serving;

    var playerA = createDiv().html(match.PlayerA.Name).addClass('scorePlayer').addAsColumn(rowA);
    var playerB = createDiv().html(match.PlayerB.Name).addClass('scorePlayer').addAsColumn(rowB);

    if (serving == "PlayerA")
        playerA.addClass('scorePlayerServing');

    if (serving == "PlayerB")
        playerB.addClass('scorePlayerServing');

    var statusSets = match.Status.Sets;
    for (var i = 0; i < statusSets.length; i++) {
        var scorePlayerA = statusSets[i].PlayerA;
        var scorePlayerB = statusSets[i].PlayerB;
        var className = "scoreSet";
        if (i == statusSets.length - 1)
            className = "scoreActiveSet";

        createDiv().html(scorePlayerA).addClass(className).addAsColumn(rowA);
        createDiv().html(scorePlayerB).addClass(className).addAsColumn(rowB);
    }

    if (match.Status.ActiveGame) {
        createDiv().html(GetGameScorePlayerA(match)).addClass('scoreGame').addAsColumn(rowA);
        createDiv().html(GetGameScorePlayerB(match)).addClass('scoreGame').addAsColumn(rowB);
    }

    table.appendTo(div);

    return div;
};


var AddColumn = function(row, content) {
    createColumn().html(content).appendTo(row);
};

(function($) {
    $.fn.addColumn = function(content) {
        AddColumn(this, content);
        return this;
    };

    $.fn.addAsColumn = function(row) {
        AddColumn(row, this);
        return this;
    };
})(jQuery);

function GetMainMatchStatus(reply) {
    return reply.MatchStatus;
}

function GetGameScorePlayerA(reply) {
    var score = reply.Status.ActiveGame.PlayerA;
    return TranslateScore(score);
}

function GetGameScorePlayerB(reply) {
    var score = reply.Status.ActiveGame.PlayerB;
    return TranslateScore(score);
}

function TranslateScore(score) {
    if (score == "Love")
        return "0";
    if (score == "Value15")
        return "15";
    if (score == "Value30")
        return "30";
    if (score == "Value40")
        return "40";
    if (score == "Advantage")
        return "Adv.";

    return null;
}