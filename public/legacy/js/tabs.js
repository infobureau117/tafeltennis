var runSlideShow = false;
var slideShowTimer = null;
var activeTab = 0;


var clickTab = function(tabIndex) {
    startTab(tabIndex);
    stopSlideShow();
    $('#playButton').fadeIn();
};

var startTab = function(tabIndex) {
    var current = $('.tab:eq(' + tabIndex + ')');

    $('.tab')
        .not(current.id)
        .fadeOut(0,
            function() {
                $(this).css('visibility', 'collapsed');
            });

    $('#nav li').removeClass('current');

    $('#nav li:eq(' + tabIndex + ')').addClass('current');

    current.css('visibility', 'visible')
        .fadeTo('fast', 1);

    activeTab = tabIndex;
};

var startSlideShow = function() {
    slideShowTimer = setTimeout(nextSlide, 8000);
    runSlideShow = true;
    $('#playButton').fadeOut();
};

var stopSlideShow = function() {
    runSlideShow = false;
    clearTimeout(slideShowTimer);
};

var nextSlide = function() {
    if (runSlideShow == false)
        return;

    var numberOfTabs = $('.tab').length;
    activeTab++;
    if (activeTab >= numberOfTabs)
        activeTab = 0;

    startTab(activeTab);

    startSlideShow();
};