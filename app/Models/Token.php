<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
	//
	
	protected $fillable = ['user_id','status','platform','device_name','token','device_id'];

}