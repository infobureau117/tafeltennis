<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Token;

class AccountController extends Controller
{
	public function index()
	{
		$users = \DB::table('users')->get();
		foreach ($users as $user) {
			$user->image = 'https://tt.bureau117.nl/players/' . $user->slug .'.jpg';
		}
		return response()->json($users);
	}

	public function show($id)
	{
		$user = \DB::table('users')->where('rferee_id', $id)->first();
		$user->image = 'https://tt.bureau117.nl/players/' . $user->slug .'.jpg';
		return response()->json($user);
	}
	
	public function devices(Request $request)
	{
		$token = new Token();
		$token->fill($request->all());
		
		if ($token->save()) {
			return response()->json(['status' => 200]);
		}
	}	
}