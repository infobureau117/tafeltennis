<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Token;
use App\Models\Invite;

class MatchController extends Controller
{
	public function index(Request $request)
	{
		$q = Invite::orderBy('created_at', 'desc');
		if ($request->has('user_id')) {
			$q->where('sender_id', $request->get('user_id'));
			$q->orWhere('sender_id', $request->get('user_id'));
		}
		return $q->paginate(25);
	}

	public function invite(Request $request)
	{
		$invite =  new Invite();
		$invite->fill($request->all());
		if ($invite->save()) {
			return response()->json(['status'=> 200]);
		}
	}
}
