<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    
	
	return $request->user();
    
    
});

Route::get('/accounts', 'AccountController@index');
Route::get('/accounts/{id}', 'AccountController@show');
Route::post('/accounts/devices', 'AccountController@devices');
Route::post('/matches/invite', 'MatchController@invite');
Route::get('/matches', 'MatchController@index');
